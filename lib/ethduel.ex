defmodule Ethduel do
  use Application
  require Prometheus.Registry

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      worker(ConCache, [[ttl_check: :timer.seconds(25), ttl: :timer.minutes(2)], [name: :currentplayers]], id: :currentplayers),
      worker(ConCache, [[ttl_check: :timer.seconds(120), ttl: :timer.minutes(45)], [name: :currentgames]], id: :currentgames),
      worker(ConCache, [[ttl_check: :timer.seconds(25), ttl: :timer.minutes(5)], [name: :recentgames]], id: :recentgames),   
      worker(ConCache, [[ttl_check: :timer.seconds(25), ttl: :timer.minutes(45)], [name: :currentgamesinfo]], id: :currentgamesinfo),  
      worker(ConCache, [[ttl_check: :timer.hours(4), ttl: :timer.hours(48)], [name: :ethconversionprices]], id: :ethfiatconversionprice),        
      worker(Ethduel.Scheduler, []),       
      supervisor(Ethduel.Repo, []),
      supervisor(Ethduel.Web.Endpoint, []),
      supervisor(Task.Supervisor, [[name: Ethduel.TaskSupervisor]]),
      supervisor(Ethduel.Presence, []),
      #  supervisor(EvercamMedia.Snapshot.WorkerSupervisor, []),
      # Start your own worker by calling: Ethduel.Worker.start_link(arg1, arg2, arg3)
      # worker(Ethduel.Worker, [arg1, arg2, arg3]),
    ]
     
    # Start Promethus Collectors
    Ethduel.PhoenixInstrumenter.setup()  
    Ethduel.PipelineInstrumenter.setup()  
    Ethduel.RepoInstrumenter.setup()  
    Prometheus.Registry.register_collector(:prometheus_process_collector)  
    Ethduel.PrometheusExporter.setup()  

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ethduel.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
end
