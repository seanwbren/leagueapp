defmodule Ethduel.RiotApi do

  @regions_and_platform_ids %{
    br1: "BR1",
    eun1: "EUN1",
    euw1: "EUW1",
    kr: "KR",
    jp1: "JP1",
    la1: "LA1",
    la2: "LA2",
    na1: "NA1",
    oc1: "OC1",
    tr1: "TR1",
    ru: "RU",
    pbe1: "PBE1"
  }

  @doc """
  Returns true is the region is valid.
  """

  def valid_region?(region) do
    Map.has_key?(@regions_and_platform_ids, String.to_atom(region))
  end

  @doc """
  Return true if all keys in check_map exist in the allowed_map
  """

  def valid_keys?(check_map, allowed_map) do
    check_keys = Map.keys(check_map)
    allowed_keys = Map.keys(allowed_map)
    
    Enum.empty?(check_keys -- allowed_keys)
  end  

  @doc """
  Returns platform id based on the region
  """

  def get_platform_id(region) do
    { :ok, platform_id } = Map.fetch(@regions_and_platform_ids, String.to_atom(region))
    platform_id
  end

  @doc """
  Returns the base url based on region
  """

  def base_url_region(region) do
    "https://" <> region <> ".api.riotgames.com"
  end

  @doc """
  Returns the global url
  """

  def base_url_global() do
    "https://global.api.riotgames.com"
  end

  @doc """
  Returns the api_key in REST format, set via environmental variable "RIOT_API_KEY"
  """

  def url_key() do
    "api_key=" <> System.get_env("RIOT_API_KEY")
  end

  @doc """
  Returns a map with the first key removed to flatten data structure.
  Return format is {:ok, data}
  """

  def strip_key(map) do
    striped_key = 
      Map.keys(map)
      |> List.first
    
    Map.fetch(map, striped_key)   
  end

  @doc """
  Returns a map with the first key removed to flatten data structure
  """

  def strip_key!(map) do
    { :ok, data } = strip_key(map)
    data
  end

  @doc """
  Verifies the requested options are valid and formats them for REST api.
  """

  def url_opts(map, defaults) do
    merge_defaults(map, defaults)
    |> remove_nils
    |> gen_opt_list
    |> concat_opts
  end

  # Merge map with defaults, use defaults where no value assigned to key
  def merge_defaults(map, defaults) do
    Map.merge(defaults, map, fn _key, default, val -> val || default end)
  end
  
  # Removes entries with nil values, returns a list of tuples
  def remove_nils(map) do
    Enum.filter map, fn { _key, val } -> val end
  end
  
  # Transform opts into rest friendly strings
  def gen_opt_list(list) do
    Enum.reduce list, [], fn { key, val }, acc ->
      acc ++ ["#{key}=#{val}"]
    end
  end
  
  # Combine opts into a single string
  def concat_opts(list) do
    Enum.join(list, "&") <> "&"
  end

  @doc """
  Handles and transforms response from API.
  """

  def handle_response({ :ok, %{status_code: 200, body: body}}) do
    {:ok, Poison.Parser.parse!(body)}
  end
  
  def handle_response({_,   %{status_code: _,   body: body}}) do
    {:error, Poison.Parser.parse!(body)}
  end

#
#
#
#Summoner Functions
#
#
#
#

  @api_version    3

  @moduledoc """
  Uses v#{@api_version} API.
  """

  @doc """
  Contains summoner data; retrieved by summoner name.
  
  Sample output:
      {:ok,
       %{"jrizznezz" => %{"id" => 51666047, "name" => "jrizznezz",
           "profileIconId" => 1450, "revisionDate" => 1488042401000,
           "summonerLevel" => 30}}}
  """

  def summoner_by_name(region, summoner_name) do
    if String.match?(summoner_name, ~r/^[0-9\p{L} _\.]+$/u) and valid_region?(region) do
      region 
      |> url_summoner_by_name(remove_spaces(summoner_name))
      |> HTTPoison.get
      |> handle_response
    else
      {:error, "invalid request"}
    end
  end

  @doc """
  Contains summoner data; retrieved by summoner id.
  
  Sample output:
      {:ok,
       %{"51666047" => %{"id" => 51666047, "name" => "jrizznezz",
           "profileIconId" => 1450, "revisionDate" => 1488042401000,
           "summonerLevel" => 30}}}
  """

  def summoner_by_id(region, summoner_id) do
    if valid_region?(region) do
      region
      |> url_summoner_by_id(summoner_id)
      |> HTTPoison.get
      |> handle_response     
    else
      {:error, "invalid request"}
    end
  end

  @doc """
  Contains summoner name; retrieved by summoner id.
  
  Sample output:
      {:ok, %{"51666047" => "jrizznezz"}}

  """

  def name(region, summoner_id) do
    if valid_region?(region) do
      region
      |> url_name(summoner_id)
      |> HTTPoison.get
      |> handle_response      
    else
      {:error, "invalid request"}
    end
  end

  @doc """
  Contains summoner id; retrieved by summoner name.
  
  Sample output:
      51666047

  """

  def id_by_name(region, name) do
    with {:ok, summoner_data } <- summoner_by_name(region, name)
    do
      {:ok, Map.fetch!(summoner_data, "id")}
    else
      {:error, _} -> {:error, "summoner not found"}
    end
  end

  def remove_spaces(str), do: String.replace(str, " ", "")
  
  def url_summoner_by_name(region, summoner) do
    base_url_region(region) 
    <> "/lol/summoner/v#{@api_version}/summoners/by-name/#{summoner}?"
    <> url_key()
    
  end

  def url_summoner_by_id(region, id) do
    base_url_region(region)
    <> "/lol/v#{@api_version}/summoner/#{id}?"
    <> url_key()
  end
  
  def url_name(region, id) do
    base_url_region(region)
    <> "/lol/v#{@api_version}/summoner/#{id}/name?"
    <> url_key()
  end
  
    


  #
  #
  #
  #
  # GAME FUNCTIONS
  #
  #
  #
  #
  #


  @doc """
  Contains game data for a player's current game; retrieved by player id.
  
  Sample output:  
      {:ok,
       %{"bannedChampions" => [%{"championId" => 114, "pickTurn" => 1,
            "teamId" => 100},
          %{"championId" => 17, "pickTurn" => 2, "teamId" => 200},
          %{"championId" => 164, "pickTurn" => 3, "teamId" => 100},
          %{"championId" => 107, "pickTurn" => 4, "teamId" => 200},
          %{"championId" => 7, "pickTurn" => 5, "teamId" => 100},
          %{"championId" => 126, "pickTurn" => 6, "teamId" => 200}],
         "gameId" => 2434912258, "gameLength" => 368, "gameMode" => "CLASSIC",
         "gameQueueConfigId" => 420, "gameStartTime" => 1487968292999,
         "gameType" => "MATCHED_GAME", "mapId" => 11,
         "observers" => %{"encryptionKey" => "MCZnljIAgQqSfRhxnSDGhsgTe3r396mj"},
         "participants" => [%{"bot" => false, "championId" => 117,
            "masteries" => [%{"masteryId" => 6211, "rank" => 5},
             %{"masteryId" => 6223, "rank" => 1},
             %{"masteryId" => 6232, "rank" => 5},
             %{"masteryId" => 6241, "rank" => 1},
             %{"masteryId" => 6311, "rank" => 5},
             %{"masteryId" => 6322, "rank" => 1},
             %{"masteryId" => 6332, "rank" => 5},
             %{"masteryId" => 6342, "rank" => 1},
             %{"masteryId" => 6352, "rank" => 5},
             %{"masteryId" => 6363, "rank" => 1}], "profileIconId" => 1301,
            "runes" => [%{"count" => 1, "runeId" => 5053},
             %{"count" => 9, "runeId" => 5273},
             %{"count" => 1, "runeId" => 5289},
             %{"count" => 7, "runeId" => 5296},
             %{"count" => 3, "runeId" => 5317},
             %{"count" => 6, "runeId" => 5320},
             %{"count" => 3, "runeId" => 5357}], "spell1Id" => 4,
            "spell2Id" => 3, "summonerId" => 72469211,
            "summonerName" => "Dubaya is Back", "teamId" => 100}, ...
  """

  def current(region, player_id) do
    if valid_region?(region) do
      region
      |> url_current(player_id)
      |> HTTPoison.get
      |> handle_response
    else
      {:error, "invalid request"}
    end
  end

  @doc """
  Contains game data for multiple featured games.
  
  Sample output:
      {:ok,
       %{"clientRefreshInterval" => 300,
         "gameList" => [%{"bannedChampions" => [], "gameId" => 2434932035,
            "gameLength" => 122, "gameMode" => "ARAM",
            "gameQueueConfigId" => 65, "gameStartTime" => 1487970919876,
            "gameType" => "MATCHED_GAME", "mapId" => 12,
            "observers" => %{"encryptionKey" => "ntCf4aznxDjgXZNWsypBIrqZTd+XwYbG"},
            "participants" => [%{"bot" => false, "championId" => 40,
               "profileIconId" => 709, "spell1Id" => 3, "spell2Id" => 4,
               "summonerName" => "Oceanman93", "teamId" => 100},
             %{"bot" => false, "championId" => 59, "profileIconId" => 551,
               "spell1Id" => 32, "spell2Id" => 4,
               "summonerName" => "Coldstream Guard", "teamId" => 100}, ...
  """

  def featured(region) do
    if valid_region?(region) do
      region
      |> url_featured
      |> HTTPoison.get
      |> handle_response
    else
      {:error, "invalid request"}
    end
  end
    
  def url_current(region, player_id) do
    base_url_region(region)
    <> "/lol/spectator/v#{@api_version}/active-games/by-summoner/#{player_id}?"
    <> url_key()
  end

  def url_featured(region) do
    base_url_region(region)
    <> "/lol/spectator/v#{@api_version}/featured-games?"
    <> url_key()
  end

  #
  #
  #
  # MATCH BY ID
  #
  #
  #


   @doc """
  Contains match data; retrieved by match id.
  Default opts:
  * includeTimeline: false
      * Flag indicating whether or not to include match timeline data
  Sample output:
      {:ok,
       %{"mapId" => 11, "matchCreation" => 1487968224848,
         "matchDuration" => 1904, "matchId" => 2434912258,
         "matchMode" => "CLASSIC", "matchType" => "MATCHED_GAME",
         "matchVersion" => "7.4.176.9828",
         "participantIdentities" => [%{"participantId" => 1,
            "player" => %{"matchaccountUri" => "/v1/stats/player_account/NA1/231272990",
              "profileIcon" => 1301, "summonerId" => 72469211,
              "summonerName" => "Dubaya is Back"}},
          ...
          ],
          "participants" => [%{"championId" => 117,
             "highestAchievedSeasonTier" => "DIAMOND",
             "masteries" => [%{"masteryId" => 6211, "rank" => 5},
              %{"masteryId" => 6223, "rank" => 1},
              %{"masteryId" => 6232, "rank" => 5},
              %{"masteryId" => 6241, "rank" => 1},
              %{"masteryId" => 6311, "rank" => 5},
              %{"masteryId" => 6322, "rank" => 1},
              %{"masteryId" => 6332, "rank" => 5},
              %{"masteryId" => 6342, "rank" => 1},
              %{"masteryId" => 6352, "rank" => 5},
              %{"masteryId" => 6363, "rank" => 1}], "participantId" => 1,
             "runes" => [%{"rank" => 1, "runeId" => 5053},
              %{"rank" => 9, "runeId" => 5273},
              %{"rank" => 1, "runeId" => 5289},
              %{"rank" => 7, "runeId" => 5296},
              %{"rank" => 3, "runeId" => 5317},
              %{"rank" => 6, "runeId" => 5320},
              %{"rank" => 3, "runeId" => 5357}], "spell1Id" => 4,
             "spell2Id" => 3,
             "stats" => %{"firstInhibitorKill" => false,
               "totalDamageTaken" => 15553, "neutralMinionsKilled" => 0,
               "kills" => 0, "totalScoreRank" => 0,
               "totalTimeCrowdControlDealt" => 524, "wardsKilled" => 6,
               "physicalDamageDealt" => 7104, "towerKills" => 0,
               "trueDamageTaken" => 121, "magicDamageDealtToChampions" => 6105,
               "totalDamageDealtToChampions" => 8415, "combatPlayerScore" => 0,
               "largestKillingSpree" => 0, "sightWardsBoughtInGame" => 0,
               "item5" => 0, "firstBloodAssist" => false,
               "largestCriticalStrike" => 0, "totalHeal" => 4589,
               "visionWardsBoughtInGame" => 3, "tripleKills" => 0,
               "objectivePlayerScore" => 0, "firstTowerAssist" => false,
               "winner" => false, "trueDamageDealt" => 2049,
               "inhibitorKills" => 0, "largestMultiKill" => 0,
               "firstInhibitorAssist" => false, "item1" => 3504,
               "neutralMinionsKilledTeamJungle" => 0, "minionsKilled" => 24,
               ...}, "teamId" => 100,
             "timeline" => %{"creepsPerMinDeltas" => %{"tenToTwenty" => 1.0,
                 "twentyToThirty" => 1.2000000000000002, "zeroToTen" => 0.1},
               "csDiffPerMinDeltas" => %{"tenToTwenty" => -1.3499999999999996,
                 "twentyToThirty" => -1.1500000000000004, "zeroToTen" => -1.4},
               "damageTakenDiffPerMinDeltas" => %{"tenToTwenty" => -259.35,
                 "twentyToThirty" => -95.10000000000002,
                 "zeroToTen" => 1.049999999999983},
               "damageTakenPerMinDeltas" => %{"tenToTwenty" => 241.1,
                 "twentyToThirty" => 832.4, "zeroToTen" => 261.5},
               "goldPerMinDeltas" => %{"tenToTwenty" => 314.6,
                 "twentyToThirty" => 318.6, "zeroToTen" => 167.0},
               "lane" => "BOTTOM", "role" => "DUO_SUPPORT",
               "xpDiffPerMinDeltas" => %{"tenToTwenty" => 46.349999999999994,
                 "twentyToThirty" => -4.850000000000023,
                 "zeroToTen" => -42.40000000000002},
               "xpPerMinDeltas" => %{"tenToTwenty" => 373.8,
                 "twentyToThirty" => 611.8, "zeroToTen" => 232.4}}},
           ...
           ],
           "platformId" => "NA1", "queueType" => "TEAM_BUILDER_RANKED_SOLO",
           "region" => "NA", "season" => "PRESEASON2017",
           "teams" => [%{"bans" => [%{"championId" => 114, "pickTurn" => 1},
               %{"championId" => 164, "pickTurn" => 3},
               %{"championId" => 7, "pickTurn" => 5}], "baronKills" => 0,
              "dominionVictoryScore" => 0, "dragonKills" => 3,
              "firstBaron" => false, "firstBlood" => false,
              "firstDragon" => true, "firstInhibitor" => false,
              "firstRiftHerald" => false, "firstTower" => false,
              "inhibitorKills" => 0, "riftHeraldKills" => 0, "teamId" => 100,
              "towerKills" => 2, "vilemawKills" => 0, "winner" => false},
            %{"bans" => [%{"championId" => 17, "pickTurn" => 2},
               %{"championId" => 107, "pickTurn" => 4},
               %{"championId" => 126, "pickTurn" => 6}], "baronKills" => 1,
              "dominionVictoryScore" => 0, "dragonKills" => 1,
              "firstBaron" => true, "firstBlood" => true, "firstDragon" => false, 
              "firstInhibitor" => true, "firstRiftHerald" => false,
              "firstTower" => true, "inhibitorKills" => 3,
              "riftHeraldKills" => 0, "teamId" => 200, "towerKills" => 11,
              "vilemawKills" => 0, "winner" => true}]}}
  """

   def match_by_id(region, match_id) do

    if valid_region?(region) do
      region
      |> url_match_by_id(match_id)
      |> HTTPoison.get
      |> handle_response
    else
      {:error, "invalid request"}
    end
  end

  def url_match_by_id(region, id) do
    base_url_region(region)
    <> "/lol/match/v#{@api_version}/matches/#{id}?"
    <> url_key()
  end
end
