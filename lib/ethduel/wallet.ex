defmodule Ethduel.Wallet do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ethduel.Wallet

  @primary_key {:address, :string, autogenerate: false}
  @derive {Phoenix.Param, key: :address}

  schema "wallets" do
    field :balance, :decimal, default: 0, precision: 26# in Wei
    field :default_region, :string, default: "na1"

    # associations
    has_many :deposits, Ethduel.Deposit, foreign_key: :address
    has_many :withdrawals, Ethduel.Withdrawal, foreign_key: :address
    has_many :wagers, Ethduel.Wager, foreign_key: :address

    timestamps()
  end


  @doc false
  def changeset(%Wallet{} = wallet, attrs) do
  wallet
  |> cast(attrs, [:address, :balance, :default_region])
  |> validate_required(:address)
  |> unique_constraint(:address)

  end

end

