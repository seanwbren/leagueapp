defmodule Ethduel.Deposit do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ethduel.Deposit


  schema "deposits" do
    field :amount, :decimal, precision: 26 # in Wei
    field :tx_status, :string, default: "pending" # can be "pending" , "failed" , or "success"
    field :tx_hash, :string
    
    belongs_to :wallet, Ethduel.Wallet, references: :address, foreign_key: :address, primary_key: true, type: :string
    

    timestamps()
  end


  @doc false
  def changeset(%Deposit{} = deposit, attrs) do
    deposit
    |> cast(attrs, [:amount])
    |> validate_required([:address, :amount])
  end


end
