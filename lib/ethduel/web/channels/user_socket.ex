defmodule Ethduel.Web.UserSocket do
  use Phoenix.Socket
  alias Ethduel.Util

  ## Channels
  channel "ethduel:users", Ethduel.Web.EthduelChannel
  channel "game:*", Ethduel.Web.GameChannel
  channel "wallet:*", Ethduel.Web.WalletChannel

  ## Transports
  transport :websocket, Phoenix.Transports.WebSocket
  # transport :longpoll, Phoenix.Transports.LongPoll

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.

  def connect(params, socket) do
    case Util.validate(params["walletAddress"]) do
      {:valid, _} ->
        {:ok, assign(socket, :walletAddress, params["walletAddress"])}
      _ ->
        {:ok, socket}
    end
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "users_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     Ethduel.Endpoint.broadcast("users_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(_socket), do: nil
  #do: "users_socket:#{socket.assigns.wallet_address}"

  # Disconnect all user's socket connections and their multiplexed channels
  #Ethduel.Endpoint.broadcast("users_socket:" <> walletAddress, "disconnect", %{})

end
