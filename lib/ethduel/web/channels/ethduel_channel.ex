defmodule Ethduel.Web.EthduelChannel do
  use Ethduel.Web, :channel
  alias Ethduel.Web.LayoutView
  alias Ethduel.Web.GameView
  alias Phoenix.View

  def join("ethduel:users", _params, socket) do
    :timer.send_interval(2000, :checkForNewWallet)
    socket = assign(socket, :metamaskbool, false)
    send(self, {:after_join})
    {:ok, socket}
  end

  # 2 second ping checks for current Metamask wallet and will reassign params["walletAddress"]
  def handle_info(:checkForNewWallet, socket) do
    push socket, "checkForNewWallet", %{msg: nil}
    {:noreply, socket}
  end

  def handle_info({:after_join}, socket) do
    push socket, "user_alert", %{
      message: "Welcome to the Alpha Version of Ethduel.  Only Usable for Testing"
    }
    {:noreply, socket}
    end


  def handle_info(_msg, state) do
  {:noreply, state}
  end

end