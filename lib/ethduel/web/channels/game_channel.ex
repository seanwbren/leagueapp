defmodule Ethduel.Web.GameChannel do
  use Ethduel.Web, :channel
  require Logger
  alias Ethduel.{RiotUtil, Chat, Game, Repo, Presence}

  # params are {"gameId": gameId, "gameTime": "unknown", "walletAddress": walletAddress}
  def join("game:" <> game_id, params, socket) do
      :timer.send_interval(1000, :incrementSecond)

      game_id = String.to_integer(game_id)
      game = Repo.get!(Ethduel.Game, game_id)
      chats = Repo.all(
        from a in assoc(game, :chats),
          order_by: [asc: a.id],
            limit: 200)
      resp = %{chats: Phoenix.View.render_many(chats, Ethduel.ChatView, "chat.json")}
      
      socket = assign(socket, :gameId, game_id)
      send(self, :after_join)
      {:ok, resp, assign(socket, :gameTime, params["gameTime"])}
  end

  def handle_info(:incrementSecond, socket) do
    game_time = socket.assigns[:gameTime]
    if game_time == -999 do 
      push socket, "incrementSecond", %{gameTime: game_time}
      {:noreply, socket}  # keep checking but do nothing
    else
      push socket, "incrementSecond", %{gameTime: game_time}
      {:noreply, assign(socket, :gameTime, game_time + 1)}
    end
  end

  # params are {body: message.val(), nickname: web3.eth.accounts[0].slice(0,7)}
  def handle_in("new_message", params, socket) do
    nickname = params["nickname"]
    body = params["body"]
    game = Repo.get(Game, socket.assigns.gameId)
    changeset = 
      game
      |> build_assoc(:chats, riot_game_id: socket.assigns.gameId)
      |> Chat.changeset(params)
    
    case Repo.insert(changeset) do
      {:ok, chat} -> 
        broadcast! socket, "new_message", %{
          id: chat.id,
          nickname: nickname,
          body: body
        }
        {:reply, :ok, socket}

      {:error, changeset} -> 
        {:reply, {:error, %{errors: changeset}}, socket}
      end
  end

  # Presence tracking for count of users in game lobby
  # def handle_info(:after_join, socket) do
  #   Presence.track(socket, socket.assigns[:walletAddress], %{
  #     online_at: :os.system_time(:milli_seconds),
  #   })
  #   push socket, "presence_state", Presence.list(socket)
  #   {:noreply, socket}
  # end
  
  def handle_info(_msg, state) do
  {:noreply, state}
  end

  def terminate(reason, socket) do
    Logger.debug"> leave #{inspect reason}"
    :ok
  end
end