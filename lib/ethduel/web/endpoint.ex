defmodule Ethduel.Web.Endpoint do
  use Phoenix.Endpoint, otp_app: :ethduel

  socket "/socket", Ethduel.Web.UserSocket

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/", from: :ethduel, gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt)

  plug Plug.RequestId

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Poison

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_ethduel_key",
    signing_salt: "l7tE9ZSW"
  
    # makes the /metrics URL happen on prod
  if :os.type == {:unix, :linux} do
    plug Ethduel.PrometheusExporter  
  end
  
  plug Ethduel.PipelineInstrumenter   # measures pipeline exec times  
  # Add Timber plugs for capturing HTTP context and events
  plug Timber.Integrations.SessionContextPlug
  plug Timber.Integrations.HTTPContextPlug
  plug Timber.Integrations.EventPlug

  plug Ethduel.Web.Router
end
