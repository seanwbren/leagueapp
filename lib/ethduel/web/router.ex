defmodule Ethduel.Web.Router do
  use Ethduel.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    #plug :Ethduel.Metamask, repo: Ethduel.Repo
    #plug :put_wallet_token
  end


  scope "/", Ethduel.Web do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/name", NameController, only: [:create]
    get "/game/random", GameController, :random
    get "/game/:gameid", GameController, :view
    get "/account", AccountController, :index
    get "/account/:walletAddress", AccountController, :view
    get "/help", HelpController, :index
  end

end
