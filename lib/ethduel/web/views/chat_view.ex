defmodule Ethduel.ChatView do
  use Ethduel.Web, :view

  def render("chat.json", %{chat: chat}) do
    %{
      id: chat.id,
      body: chat.body,
      nickname: chat.nickname
    }
  end
end