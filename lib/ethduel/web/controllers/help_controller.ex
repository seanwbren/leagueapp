defmodule Ethduel.Web.HelpController do
  use Ethduel.Web, :controller
  require Logger

  def index(conn, _params) do
    render(conn, "help.html")
  end
end