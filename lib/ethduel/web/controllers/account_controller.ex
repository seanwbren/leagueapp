defmodule Ethduel.Web.AccountController do
  use Ethduel.Web, :controller
  require Logger

  def index(conn, _params) do
    render(conn, "account.html")
  end

   def view(conn, %{"walletAddress" => walletAddress}) do
    render(conn, "account.html", walletAddress: walletAddress)
  end
end