defmodule Ethduel.Web.NameController do
  use Ethduel.Web, :controller
  require Logger
  require ConCache
  alias Ethduel.{RiotUtil, Game}

  def index(conn, _params) do
    conn |> put_flash(:error, "Redirecting to homepage") |> redirect(to: "/") |> halt()
  end

  def create(conn, _params) do
    region = conn.params["parameters"]["region"]
    name = conn.params["parameters"]["name"]
    gameId = RiotUtil.checkNameInCache(region, name)
    if gameId do 
      IO.puts("#{name} in cache, redirecting without API call")
      redirect conn, to: "/game/#{gameId}"
    else 
      with {:ok, gameInfo} <- RiotUtil.getGameFromName(region, name) do
        gameId = RiotUtil.getGameIdFromGame(gameInfo)
        Game.insertGame(gameId, gameInfo, region)
        RiotUtil.putDataInConCache(region, gameInfo)
        redirect conn, to: "/game/#{gameId}"
      else 
        {:error, _} ->
          conn
          |> put_flash(:error, "#{name} not found in a #{region} region game.") 
          |> redirect(to: "/")
      end
    end
  end
end


