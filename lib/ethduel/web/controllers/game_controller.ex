defmodule Ethduel.Web.GameController do
  use Ethduel.Web, :controller
  require Logger
  require ConCache
  alias Ethduel.{RiotUtil, Repo, Game}
  import Ecto.Query

  def index(conn, _params) do
    conn |> put_flash(:error, "Redirecting to homepage") |> redirect(to: "/") |> halt()
  end
  
  # check the database for teams, then check cache for updated time info.
  def view(conn, %{"gameid" => gameid}) do

    #get teams from Ecto
    query = from p in Game, 
            where: p.riot_game_id == ^gameid, 
            select: p.champions
    teams = List.first(Repo.all(query))

    #if that query fails due to DB load could fallback to teams = RiotUtil.getCachedTeamsFromID(gameid).  Don't know if its worth keeping that cache around

    #get the true game time
    {gameLength, trueGameLength} = RiotUtil.getTrueGameLength(gameid)
    if gameLength == 0, do: gameTime = -999, else: gameTime = trueGameLength
    
    render(conn, "leaguegame.html", gameid: gameid, teams: teams, gameTime: gameTime)
  end

  def random(conn, _param) do
    with {:ok, gameId} <- RiotUtil.getRandomRecentGame() do
      redirect conn, to: "/game/#{gameId}"
    else
      {:error, reason} -> 
        conn 
        |> put_flash(:error, "No recent games found. Try searching for a player!") 
        |> redirect(to: "/")
      end
  end
end


    
    
