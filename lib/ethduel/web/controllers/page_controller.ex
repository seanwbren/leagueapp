defmodule Ethduel.Web.PageController do
  use Ethduel.Web, :controller
  alias Ethduel.RiotUtil

  def index(conn, _params) do
    #recentgames = RiotUtil.getRecentGamesList()
    populargames = [["4", "24", "23", "11", "12", "15", "27", "28", "33", "36"], ["40", "41", "23", "42", "43", "45", "28", "48", "50", "51"], ["53", "54", "55", "56", "57", "58", "59", "60", "61", "62"], ["63", "64", "67", "81", "80", "82", "83", "84", "85", "86"], ["89", "90", "91", "92", "96", "101", "102", "103", "104", "105"]]
    render(conn, "index.html", populargames: populargames)
  end

  def view(conn, _params) do
    render(conn, "index.html")
  end

end
