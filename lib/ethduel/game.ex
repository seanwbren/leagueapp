defmodule Ethduel.Game do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ethduel.{Game, RiotUtil, Repo}

  @primary_key {:riot_game_id, :decimal, autogenerate: false}
  @derive {Phoenix.Param, key: :riot_game_id}


  schema "games" do
    field :champions, {:array, {:array, :string}}
    field :region, :string
    field :final_game_info, :map # from RIOT match api 
    field :winning_team, :string, default: nil 
    field :time_ended, :naive_datetime, default: nil
    field :remake, :boolean
    has_many :wagers, Ethduel.Wager, foreign_key: :riot_game_id
    has_many :chats, Ethduel.Chat, foreign_key: :riot_game_id

    timestamps()
  end

  @doc false
  def changeset(%Game{} = game, attrs) do
    game
    |> cast(attrs, [:riot_game_id, :champions, :region, :final_game_info, :winning_team, :time_ended, :remake])
    |> validate_required([:riot_game_id, :champions, :region])
  end

  # integer, list, string
  def insertGame(game_id, gameInfo, region) do
    teams = RiotUtil.getTeamList(gameInfo)
    changeset = Game.changeset(%Game{}, %{riot_game_id: game_id,
                                          champions: teams,
                                          region: region})
    case Repo.insert(changeset, on_conflict: :nothing) do
      {:ok, record}       -> IO.inspect(record)
      {:error, changeset} -> IO.inspect(changeset)
    end  
  end

end
