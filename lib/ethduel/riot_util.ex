defmodule Ethduel.RiotUtil do
    import Ethduel.RiotApi
    import ConCache
    import Kernel
    #import ExRated

    api_key = Confex.get(:ethduel, :riot_api_key)

    #
    # PlAYER NAME FUNCTIONS
    #

    def getSummonerIdFromName(region, name) do
      with {:ok, playerId} <- Ethduel.RiotApi.id_by_name(region, name) 
      do {:ok, playerId}
      else
        {:error, reason} -> {:error, reason}
      end
    end

    #returns nil or gameid
    def checkNameInCache(region, name) do
      lower_name = String.downcase(name)
      regionAndName = region <> lower_name
      player = ConCache.get(:currentplayers, regionAndName) # possibly nil
      player
    end
    
    #
    # GAME FUNCTIONS (used while game is ongoing)
    #


    def getGameFromId(region, playerId) do
      with {:ok, gameInfo} <- Ethduel.RiotApi.current(region, playerId) do {:ok, gameInfo}
      else {:error, reason} -> {:error, reason}
      end
    end
       
    def getGameFromName(region, name) do
      with {:ok, summonerId} <- getSummonerIdFromName(region, name),
           {:ok, gameInfo} <- getGameFromId(region, summonerId) 
      do {:ok, gameInfo}
      else {:error, reason} -> {:error, reason}
      end
    end

    def getGameIdFromGame(gameInfo) do
      with {:ok, gameId} <- Map.get(gameInfo, "gameId") do {:ok, gameId}
      end
    end

    def getTrueGameLength(gameId) do
      gameIdString = String.to_integer(gameId)
      gameInfo = ConCache.get(:currentgamesinfo, gameIdString)
      initialGameTime = Map.get(gameInfo, "gameLength")
  
      time_now = :os.system_time(:second)
      time_then =  Map.get(gameInfo, "timeCacheCreated")
      IO.puts("gameLength is #{initialGameTime} and the system time now #{time_now} and when cache created #{time_then}")
      trueGameTime = initialGameTime + time_now - time_then
      {initialGameTime, trueGameTime}
    end

    
    # Returns a list of lists like: 
    # [[74, "Kedarah"], [126, "Undead Viren"], [64, "Xifishia"],
    # [119, "Shadow League"], [99, "summit1g"], [236, "Fongshizzle"],
    # [84, "QueenKat14"], [63, "JAMelendez"], [57, "Rumble504"],
    # [75, "DatAsheThooooooo"]]
    # gameInfo is NOT the {:ok, gameInfo} tuple for the following functions
    def getTeamList(gameInfo) do 
      players = Map.get(gameInfo, "participants")
      ids = Enum.map(players, & &1["championId"])
      names = Enum.map(players, & &1["summonerName"]) 
      string_ids = Enum.map(ids, fn(x) -> Integer.to_string(x) end)
      champions = List.zip([string_ids, names])
      list_champions = nested_tuple_to_list(champions)
    end

    def getGameDetails(gameInfo) do
      Map.take(gameInfo, ["gameId", "gameLength", "gameMode", "gameQueueConfigId", 
                            "gameStartTime", "gameType", "mapId", "platformId"])
    end  

    def getGameRegion(gameInfo) do
      Map.take(gameInfo, ["platformId"])
    end
      
    #
    # CONCACHE FUNCTIONS
    #

      # Stores gameInfo data in :currentplayers and :currentgames and :recentgames and :currentgamesinfo
    def putDataInConCache(region, gameInfo) do
      gameId = Map.get(gameInfo, "gameId")
      players = Map.get(gameInfo, "participants")
      ids = Enum.map(players, & &1["championId"])
      names = Enum.map(players, & &1["summonerName"]) 

      # 1st cache stores gameInfo and time
      time_now = :os.system_time(:second)
      gameInfo = Map.put(gameInfo, "timeCacheCreated", time_now)
      ConCache.put(:currentgamesinfo, gameId, gameInfo)

      # 2nd cache stores in :currentplayers the player+region names and returns their current gameid. 
      for name <- names, do: ConCache.put(:currentplayers, region <> String.downcase(name), gameId)

      # 3rd cache stores gameids and returns the list of players and their champion. 
      string_ids = Enum.map(ids, fn(x) -> Integer.to_string(x) end)
      champions = List.zip([string_ids, names])
      list_champions = nested_tuple_to_list(champions)
      ConCache.put(:currentgames, gameId, list_champions)

      # 4th cache for 10 recent games. TTL 5 minutes and new games overwrite old
      rand_num = Enum.random(1..5)
      rand_num_string = Integer.to_string(rand_num)
      IO.puts("data put in concache and random num is" <> rand_num_string <> "for gameId" <> "#{gameId}")
      ConCache.put(:recentgames, rand_num_string, gameId)
    end

    def getCachedTeamsFromID(gameId) do
      gameIdInt = String.to_integer(gameId)  
      teams = ConCache.get(:currentgames, gameIdInt)
    end

    # random recent game functionality
    def getRandomRecentGame() do
      list = ["1","2","3", "4", "5"]
      gameIds = for n <- list, do: ConCache.get(:recentgames, n)
      gameIdsNoNils = Enum.reject(gameIds, fn(x) -> is_nil(x) end)
      randomRecentGameId(gameIdsNoNils)
    end

    defp randomRecentGameId([]) do
      {:error, "no recent games found"}
    end
    defp randomRecentGameId(gameIds) do
      random1tolength = Enum.random(0..(Kernel.length(gameIds) - 1))
      {randomGameId, _} = List.pop_at(gameIds, random1tolength)
      {:ok, randomGameId}
    end

    
  


    #
    # MATCH FUNCTIONS (after game completes)
    #

    def getMatchFromId(region, gameId) do
      with {:ok, matchInfo} <- Ethduel.RiotApi.match_by_id(region, gameId) do matchInfo
      else {:error, error} -> "error: #{error} "
      end
    end

    # returns {:ok, teamId} and teamId is TODO
    def getWinnerFromMatch(matchInfo) do
      teams = Map.get(matchInfo, "teams")
      mapWithWinningTeamId = 
      for team <- teams do
        if Map.get(team, "winner") == true do
          Map.get(team, "teamId")
        end
      end
      teamId = List.first(Enum.sort(mapWithWinningTeamId)) #will be [teamID, nil] or [nil, nil] if error
      if teamId do 
        {:ok, teamId}
      else {:error, "no winning team"}
      end
    end  

    #
    # FEATURED GAME LIST
    #

    # format is: 
    # {:ok,
    # %{"clientRefreshInterval" => 300,
    #   "gameList" => [%{"bannedChampions" => [%{"championId" => 38, "pickTurn" => 1,
    #         "teamId" => 100},
    #       %{"championId" => 92, "pickTurn" => 2, "teamId" => 100},
    #       %{"championId" => 51, "pickTurn" => 3, "teamId" => 100},
    #       %{"championId" => 29, "pickTurn" => 4, "teamId" => 100},
    #       %{"championId" => 7, "pickTurn" => 5, "teamId" => 100},
    #       %{"championId" => 29, "pickTurn" => 6, "teamId" => 200},
    #       %{"championId" => 38, "pickTurn" => 7, "teamId" => 200},
    #       %{"championId" => 113, "pickTurn" => 8, "teamId" => 200},
    #       %{"championId" => 40, "pickTurn" => 9, "teamId" => 200},
    #       %{"championId" => 412, "pickTurn" => 10, "teamId" => 200}],
    #      "gameId" => 2609459489, "gameLength" => 161, "gameMode" => "CLASSIC",
    #      "gameQueueConfigId" => 420, "gameStartTime" => 1506753477396,
    #      "gameType" => "MATCHED_GAME", "mapId" => 11,
    #      "observers" => %{"encryptionKey" => "4aSkkHG3AQs6ffzYX2qO//z93/J4iNtU"},
    #      "participants" => [%{"bot" => false, "championId" => 59,
    #         "profileIconId" => 983, "spell1Id" => 4, "spell2Id" => 12,
    #         "summonerName" => "Dubehavior", "teamId" => 100},
    #       %{"bot" => false, "championId" => 64, "profileIconId" => 18,
    #         "spell1Id" => 4, "spell2Id" => 11, "summonerName" => "Anaunymous",
    #         "teamId" => 100},
    #       %{"bot" => false, "championId" => 37, "profileIconId" => 1670,
    #         "spell1Id" => 4, "spell2Id" => 7, "summonerName" => "Amaaterasu",
    #         "teamId" => 100},
    #       %{"bot" => false, "championId" => 110, "profileIconId" => 3006,
    #         "spell1Id" => 21, "spell2Id" => 4, "summonerName" => "Xinthus",
    #         "teamId" => 100},
    #       %{"bot" => false, "championId" => 4, "profileIconId" => 3007,
    #         "spell1Id" => 4, "spell2Id" => 6, "summonerName" => "Anivia Kid",
    #         "teamId" => 100},
    #       %{"bot" => false, "championId" => 163, "profileIconId" => 1455,
    #         "spell1Id" => 4, "spell2Id" => 1, "summonerName" => "k3soju",
    #         "teamId" => 200},
    #       %{"bot" => false, "championId" => 161, "profileIconId" => 934,
    #         "spell1Id" => 14, "spell2Id" => 4, "summonerName" => "The Eggsalad",
    #         "teamId" => 200},
    #       %{"bot" => false, "championId" => 76, "profileIconId" => 1183,
    #         "spell1Id" => 11, "spell2Id" => 4, "summonerName" => "evøN",
    #         "teamId" => 200},
    #       %{"bot" => false, "championId" => 18, "profileIconId" => 2087,
    #         "spell1Id" => 7, "spell2Id" => 4, "summonerName" => "Shøryu",
    #         "teamId" => 200},
    #       %{"bot" => false, "championId" => 54, "profileIconId" => 3022,
    #         "spell1Id" => 12, "spell2Id" => 4, "summonerName" => "Chui Xiong Kou",
    #         "teamId" => 200}], "platformId" => "NA1"},
    #    %{"bannedChampions" => [], "gameId" => 2609416936,
    def getFeaturedGames(region) do
      games = Ethduel.RiotApi.featured(region)
    end


    #
    # UTILITY
    #

    def nested_tuple_to_list(tuple) when is_tuple(tuple) do
        tuple |> Tuple.to_list |> Enum.map(&nested_tuple_to_list/1)
    end
    
    def nested_tuple_to_list(list) when is_list(list) do
      list |> Enum.map(&nested_tuple_to_list/1)
    end

    def nested_tuple_to_list(x), do: x
  

  end  
 