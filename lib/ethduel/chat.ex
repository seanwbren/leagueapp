defmodule Ethduel.Chat do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ethduel.Chat


  schema "chats" do
    field :body, :string
    field :nickname, :string

    belongs_to :riot_game, Ethduel.Game, references: :riot_game_id, type: :decimal
    
    

    timestamps()
  end

  @doc false
  def changeset(%Chat{} = chat, attrs) do
    chat
    |> cast(attrs, [:body, :nickname])
    |> validate_required([:body, :nickname])
  end
end
