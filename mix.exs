defmodule Ethduel.Mixfile do
  use Mix.Project

  def project do
    [app: :ethduel,
     version: "0.1.15",
     elixir: "~> 1.5.1",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(),
     deps: deps()]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {Ethduel, []},
     applications: app_list(Mix.env)]
  end

  #defp app_list(:test), do: [: | app_list]
  defp app_list(_), do: app_list()
  defp app_list(), do: [
    :phoenix, 
    :phoenix_pubsub, 
    :phoenix_html, 
    :cowboy, 
    :logger,
    :phoenix_ecto, 
    :postgrex, 
    :edeliver, 
    :ex_rated,
    :confex, 
    :chemist,
    :calendar,
    :runtime_tools,
    :uuid,
    :phoenix_inline_svg,
    :prometheus_ex, 
    :prometheus_ecto, 
    :prometheus_phoenix, 
    :prometheus_plugs, 
    :prometheus_process_collector,
    :httpoison,
    :con_cache,
    :poison,
    :quantum,
    :timber
  ]
  
  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [{:phoenix, "~> 1.3.0-rc.2"},
     {:phoenix_pubsub, "~> 1.0"},
     {:phoenix_ecto, "~> 3.2"},
     {:postgrex, ">= 0.11.2"},
     {:phoenix_html, "~> 2.6"},
     {:distillery, "~> 1.4"},
     {:edeliver, "~> 1.4.2"},
     {:con_cache, "~> 0.12.0"},
     {:calendar, "~> 0.17.0"},
     {:chemist, "~> 0.4.0"},
     {:confex, "~> 1.5.0"},
     {:cowboy, "~> 1.0"},
     {:uuid, "~> 1.1"},
     {:phoenix_inline_svg, "~> 1.0"},
     {:prometheus_ex, "~> 1.4.1"},
     {:prometheus_ecto, "~> 1.0.2"},
     {:prometheus_phoenix, "~> 1.2"},
     {:prometheus_plugs, "~> 1.1.4"},
     {:prometheus_process_collector, "~> 1.1"},
     {:poison, "~> 3.1", override: true},
     {:httpoison, "~> 0.11.1"},
     {:shouldi, ">= 0.0.0", only: :test},
     {:quantum, ">= 2.1.0"},
     {:timber, "~> 2.6.1"},
     {:timex, "~> 3.0"},
     {:ex_rated, "~> 1.2"}
  ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
     "test": ["ecto.create --quiet", "ecto.migrate", "test"]]
  end
end
