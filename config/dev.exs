use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :ethduel, Ethduel.Web.Endpoint,
  http: [port: 4017],
  debug_errors: true,
  code_reloader: false,
  check_origin: false,
  watchers: [node: ["node_modules/brunch/bin/brunch", "watch", "--stdin",
                   cd: Path.expand("../assets", __DIR__)]]


# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20


# Configure your database
config :ethduel, Ethduel.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "ethduel_dev",
  hostname: "localhost",
  pool_size: 10,
  loggers: [Ethduel.RepoInstrumenter]
