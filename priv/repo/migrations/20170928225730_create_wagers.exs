defmodule Ethduel.Repo.Migrations.CreateWagers do
  use Ecto.Migration

  def change do
    create table(:wagers) do
      add :red_wallet, :string, null: false
      add :blue_wallet, :string, null: false
      add :amount, :decimal, null: false
      add :riot_game_id, references(:games, column: :riot_game_id, on_delete: :nothing, type: :decimal), null: false
      add :winning_team, :integer # 001 or 002.  001 is blue, 002 is red
      add :winning_wallet, references(:wallets, column: :address, on_delete: :nothing, type: :string) #winning wallet
      add :paid, :boolean, default: false, null: false

      timestamps()
    end

  end
end
