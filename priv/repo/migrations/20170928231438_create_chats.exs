defmodule Ethduel.Repo.Migrations.CreateChats do
  use Ecto.Migration

  def change do
    create table(:chats) do
      add :nickname, :string, null: false
      add :body, :text, null: false
      add :riot_game_id, references(:games, column: :riot_game_id, on_delete: :nothing, type: :decimal),  null: false

      timestamps()
    end

  end
end
