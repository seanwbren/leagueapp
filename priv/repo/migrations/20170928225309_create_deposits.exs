defmodule Ethduel.Repo.Migrations.CreateDeposits do
  use Ecto.Migration

  def change do
    create table(:deposits) do
      add :address, references(:wallets, column: :address, on_delete: :nothing, type: :string), null: false
      add :amount, :decimal, null: false
      add :tx_status, :string, default: "pending"      
      add :tx_hash, :string      

      timestamps()
    end

  end
end
