defmodule Ethduel.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
    create table(:games, primary_key: false) do
      add :riot_game_id, :decimal, primary_key: true, null: false
      add :champions, {:array, {:array, :string}}, null: false
      add :region, :string, null: false
      add :final_game_info, :map
      add :winning_team, :string
      add :time_ended, :naive_datetime
      add :remake, :boolean, default: false

      timestamps()
    end

    create unique_index(:games, [:riot_game_id])

  end
end
