defmodule Ethduel.Repo.Migrations.CreateWallets do
  use Ecto.Migration

  def change do
    create table(:wallets, primary_key: false) do
      add :address, :string, primary_key: true
      add :balance, :decimal
      add :default_region, :string

      timestamps()
    end

    create unique_index(:wallets, [:address])

  end
end
