let Game = {

  init(socket){
    let gameId = window.location.pathname.split("/").pop();
    let params = {"gameId": gameId, "gameTime": "unknown"};
    socket.connect(params);
    this.onReady(gameId, socket);
  },

onReady(gameId, socket){
  let msgContainer = document.getElementById("msg-container");
  let msgPosts = document.getElementById("chat-messages");
  let message = $("#message-input");
  let blueBackground = new Image().src = "url(/images/background/blue.svg)"; 
  let redBackground = new Image().src = "url(/images/background/red.svg)"; 
  let defaultBackground = new Image().src = "url(/images/background/bluered.svg)";    
  
  let currentGameTime = document.getElementById("game-time-now").innerHTML;
  let gameTimer = document.getElementById("game-time-now");

  let gameChannel = socket.channel("game:" + gameId, {"gameId": gameId, "gameTime": parseInt(currentGameTime)});

  document.getElementById("blue-bet-button").onclick = function(){document.getElementById("league-game-body").style.backgroundImage = "url(/images/background/blue.svg)";}  // put these in the submit bet code
  document.getElementById("red-bet-button").onclick = function(){document.getElementById("league-game-body").style.backgroundImage = "url(/images/background/red.svg)";}

  if ((typeof window.MetaMaskBool === 'undefined') || (window.MetaMaskBool == false)) {
    document.getElementById('transaction-notifications').innerHTML='<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Welcome to Ethduel (Alpha Version). Without the MetaMask browser extension you can not bet on games, so please read the "Getting Started" page to learn how to deposit Ether and bet. Good luck!</div>';
  }

  message.on('keypress', event => {
    if ($.trim(message.val()).length !== 0 && event.keyCode == 13 && typeof web3.eth.accounts[0] !== 'undefined' && window.MetaMaskBool == true) {
    let payload = {body: message.val(), nickname: web3.eth.accounts[0].slice(0,7)};
    gameChannel.push("new_message", payload)
               .receive("error", e => console.log(e) );
    message.val("");
    }
  })

  gameChannel.on("new_message", (resp) => {
    this.renderMessage(msgPosts, resp);
  })
  

  gameChannel.on("incrementSecond", ({"gameTime": game_time}) => {
    this.updateGameTime(gameTimer, game_time);
  })

  gameChannel.join()
    .receive("ok", ({chats}) => { 
      chats.forEach( chat => this.renderMessage(msgPosts, chat) )
     })
    .receive("error", resp => { console.log("Unable to join", resp) });
  },

  //
  // FUNCTIONS
  //

  esc(str){
    let div = document.createElement("div");
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
  },

  renderMessage(msgPosts, {body, nickname}){
    let template = document.createElement("div");
    template.innerHTML = `
    <b>${nickname}: ${this.esc(body)}
    `;
    template.style.textAlign = "left";
    msgPosts.appendChild(template);
    msgPosts.scrollTop = msgPosts.scrollHeight;
  },

  updateGameTime(gameTimer, gameTime){
    if (gameTime == -999){
      gameTimer.innerHTML = "Loading. Refresh after game start to see game clock.";

    } 
    else {
    var nonNegativeGameTime = gameTime + 170;
    var minutes = Math.floor(nonNegativeGameTime / 60);
    var seconds = nonNegativeGameTime % 60;
    function str_pad_left(string,pad,length) {                                  //formatting
    return (new Array(length+1).join(pad)+string).slice(-length);               //formatting
    }
    var finalTime = str_pad_left(minutes,'0',2)+':'+str_pad_left(seconds,'0',2); //formatting
    gameTimer.innerHTML = "Current Game Time: " + finalTime;
    }
  },

  fadeBlue(){
    document.getElementById("body").style.backgroundImage = blueBackground;
  },

  fadeRed(){
    document.getElementById("body").style.backgroundImage = redBackground;
  },

  fadeDefault(){
    document.getElementById("body").style.backgroundImage = defaultBackground;
  },
}
export default Game