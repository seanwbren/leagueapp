let Wallet = {
  
    init(socket, activeWallet, networkName){
      let params = {"walletAddress": activeWallet, "networkName": networkName}
      socket.connect(params)
      this.onReady(socket)
    },
  
  onReady(socket){
    let walletChannel = socket.channel("wallet:" + socket.params["walletAddress"]);
    let depositButton = $("#deposit-button-submit");
    let withdrawButton = $("#withdraw-button-submit");
    let networkName = socket.params["networkName"];
    let notificationDiv = document.getElementById('transaction-notifications');

    // header toggle functionality
    $('#eth-user-balance').hide();
    $('#eth-user-balance, #usd-user-balance').click(function(){
      $('#eth-user-balance, #usd-user-balance').toggle();
    });

    walletChannel.join()
    .receive("ok", resp => { console.log("Joined wallet channel successfully with: " + socket.params["walletAddress"], resp) })
    .receive("error", resp => { console.log("Unable to join", resp) });

    depositButton.on('click', event => {
      let depositAmount = document.getElementById("deposit-amount-input").value;
      console.log("DEPOSIT AMOUNT IS " + depositAmount)
      let currentAccount = web3.eth.accounts[0]
      let payload = {account: currentAccount, amount: depositAmount};
      
      if (depositAmount <= 0.0000001) {
        notificationDiv.innerHTML = '<div class="alert alert-warning" role="alert"> Please enter a value greater than 0.0000001 and less than 10</div>';        
      } else {
        if (networkName == '42') {
        notificationDiv.innerHTML = '<div class="alert alert-info" role="alert"> Sending a web3.eth deposit with account: "' + currentAccount + ' </div>';
        walletChannel.push("deposit_insert", payload)
                    .receive("error", e => 
                    notificationDiv.innerHTML = '<div class="alert alert-danger" role="alert"> Transaction error."' + e + '</div>'
                  );
        } else {
          notificationDiv.innerHTML = '<div class="alert alert-warning" role="alert"> You are on the wrong network. Please switch to the Kovan test network in Metamask.</div>';  
        }
      }
    }),

    walletChannel.on("metamask_deposit", (resp) => {
      console.log("deposit resp is: ", resp);
      this.initiateMetamaskDeposit(resp, walletChannel);

    }),

    walletChannel.on("update_balance", (resp) => {
      console.log("updating user balance UI with resp: ", resp);
      this.updateWalletBalanceView(resp);

    }),

    withdrawButton.on('click', event => {
      let withdrawAmount = parseInt(document.getElementById("withdraw-amount-input").value);
      let withdrawAddress = document.getElementById("withdraw-address-input").value;
      console.log("withdraw address is: " , withdrawAddress);
      let payload = {address: withdrawAddress, amount: withdrawAmount};
      if (depositAmount <= 0) {
        notificationDiv.innerHTML = '<div class="alert alert-warning" role="alert"> Please enter a value greater than 0</div>';        
      } else {
        if (networkName == '42') {
        notificationDiv.innerHTML = '<div class="alert alert-info" role="alert"> Sending a Kovan web3.eth withdrawal from account: "' + web3.eth.accounts[0] + ' to account ' + withdrawAddress + '</div>';
        walletChannel.push("withdraw", payload)
                      .receive("error", e => console.log(e) );
        } else {
          notificationDiv.innerHTML = '<div class="alert alert-warning" role="alert"> You are on the wrong network. Please switch to the Kovan test network in Metamask.</div>';  
        }
      }
    }),


    walletChannel.on("withdraw", (resp) => {
      console.log("withdraw resp is: ", resp);
      this.initiateMetamaskWithdraw(resp, walletChannel);
    })

},

  
  
  //
  // FUNCTIONS
  //

  initiateMetamaskDeposit({account, amount, id}, walletChannel){
    let gasPriceWei = "4000000000";
    let weiAmount = new web3.BigNumber(amount); // amount is already in wei
    let self = this; // this changes when entering sendTransaction
    console.log('Initiating MetaMask deposit with amount: ' + amount +  ' from ' + account);
    
    web3.eth.sendTransaction({
      from: account, 
      to:'0x8acb71431FD017AC95d5DA5Fc5D73ad72AdC877C',  // hot wallet address or contract address
      value: weiAmount, 
      gasPrice: gasPriceWei},
      function(error, txHash){
        if(error){
          document.getElementById('transaction-notifications').innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>The deposit to ' + account + ' did not complete.</div>';
          console.log(error);
        } else {
          console.log("Successful MetaMask transaction! with txHash: " + txHash);
          self.creditAccount(account, weiAmount, walletChannel);
          document.getElementById('transaction-notifications').innerHTML = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Successful Deposit: <a href="https://kovan.etherscan.io/tx/' + txHash + '" target="_blank"> View Transaction </a></div>';
        }
      
    })
  },

  creditAccount(wallet, value, walletChannel){
    let payload = {account: wallet, amount: value};
    walletChannel.push("credit_account", payload)
                 .receive("error", e => console.log(e) );
  },

  initiateMetamaskWithdraw({address, amount}, walletChannel){
    let gasPriceWei = "4000000000";
    let bigAmount = new web3.BigNumber(amount);
    let weiAmount = web3.toWei(bigAmount, 'ether');
    let currentAccount = web3.eth.accounts[0];
    let destinationAddress = address.toLowerCase();
    let self = this; // this changes when entering sendTransaction
    console.log('Initiating a MetaMask sign to start a withdrawal of amount: ' + amount +  ' from current account ' + currentAccount + ' to: ' + destinationAddress + '.');
    let textToSign = 'Withdrawal initiated of ' + amount + ' Ether from Ethduel account: ' + currentAccount + ' to Ethereum address: ' + destinationAddress;
    var hexTextToSign = this.toHex(textToSign);
    console.log(hexTextToSign);
    
    web3.personal.sign(hexTextToSign, currentAccount,
      function(error, result){
        if(error){
          document.getElementById('transaction-notifications').innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>The withdrawal to ' + destinationAddress + ' did not complete.</div>';
          return console.log(error);
        } else {
          document.getElementById('transaction-notifications').innerHTML = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Withdrawal of ' + amount + ' Ether initiated to: ' + destinationAddress + ' It will take a few minutes to transfer.</div>';
          return console.log("Successful MetaMask signature! Withdrawal initiated, it will take a few minutes for the Ether to transfer.");
          // Initiate Database Call
          // Initiate Ether Send
        }
      
    })
  },

  // either ecto_balance or change is nil/null based on whether it's in the after_join or deposit/withdrawal
  updateWalletBalanceView({ecto_balance, change, price}){
    if (ecto_balance !== null){
      document.getElementById('wei-user-balance').innerHTML= ecto_balance;  // IN WEI. this is in a hidden div

      let etherBalance = this.convertWeiToEtherForUI(ecto_balance).toFixed(6);
      document.getElementById('eth-user-balance').innerHTML= etherBalance + " ETH";

      window.EthUSDPrice = price;
      let usdBalance = this.convertToUSD(etherBalance)
      document.getElementById('usd-user-balance').innerHTML= "$" + usdBalance;

    }
    if (change !== null){  
      let bigBalance = new web3.BigNumber(document.getElementById('wei-user-balance').innerHTML);
      let bigChange = new web3.BigNumber(change);
      let newWeiBalance = bigBalance.plus(bigChange);
      document.getElementById('wei-user-balance').innerHTML= newWeiBalance;  // this is in a hidden div

      let newEtherBalance = this.convertWeiToEtherForUI(newWeiBalance).toFixed(6);
      document.getElementById('eth-user-balance').innerHTML= newEtherBalance;

      let newUSDBalance = this.convertToUSD(newEtherBalance)
      document.getElementById('usd-user-balance').innerHTML= newUSDBalance;
    }

  },

  publishAlert({msg, status}) {
    document.getElementById('transaction-notifications').innerHTML = '<div class="alert alert-' + status +  ' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+ msg + '</div>';
  },

  toHex(s) {
    var s = unescape(encodeURIComponent(s))
    var h = '0x'  // MetaMask expects hex string to start with 0x
    for (var i = 0; i < s.length; i++) {
        h += s.charCodeAt(i).toString(16)
    }
    return h
  },

  convertWeiToEtherForUI(amount) {
    let bigAmount = new web3.BigNumber(amount);
    let weiRatio = new web3.BigNumber(1000000000000000000);
    let etherAmount = bigAmount.div(weiRatio);
    return etherAmount;
  },

  convertToUSD(AmountInEther){
    let price = window.EthUSDPrice;
    let conversion = AmountInEther * price;
    let rounded_conversion = parseFloat(conversion).toFixed(2);
    return rounded_conversion
  },

  switchETHUSD(){
    $('#eth-user-balance,#usd-user-balance').click(function(){
      $('#eth-user-balance,#usd-user-balance').toggle();
  });
  }

}
export default Wallet
